package main

import (
	"flag"
)

type structFlags struct {
	IP          string
	Domain      string
	SubDomain   string
	ParUrl      string
	TmpFile     string //保存IP地址的临时文件名称
	Script      string //IP更改时执行脚本程序
	DnspodToken string //DNSPod Token ，登录 DNSPod 账号中心控制台，依次单击账号中心->密钥管理
	Version     bool   //版本
}

//var ip = flag.String("ip", "10.0.0.1", "修改记录的IP地址")
//var pDomain = flag.String("domain", "300y.com.cn", "域名")
//var subDomain = flag.String("record", "www", "修改记录的名称，如www")

var (
	f structFlags
)

func init() {
	flag.StringVar(&f.IP, "ip", "0", "修改记录的IP地址，为0或不输入此参数则由程序自动取公网IP")
	flag.StringVar(&f.Domain, "domain", "", "根域名，如taobao.com")
	flag.StringVar(&f.SubDomain, "record", "", "修改记录的名称，如www")
	flag.StringVar(&f.ParUrl, "parurl", "http://ip-api.com/json/", "通过url取得ip,必须返回的是json，并有Query字段")
	flag.StringVar(&f.DnspodToken, "p", "0", "DNSPOD调用API的token")
	flag.BoolVar(&f.Version, "version", false, "熊义写的用于dnspod的动态域名工具，QQ:34512664。\t\n 运行至少需要指定根域名、子域名、DNSPOD调用API的token。")
	flag.StringVar(&f.Script, "script", "/etc/xiongyi/dnspod.sh", "IP更新时，执行脚本")
}

//main()
//解析参数
//flag.Parse()
//pDomain := "300y.com.cn"
//subDomain := "a"
//ip := "192.168.8.8"
