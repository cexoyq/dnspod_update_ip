package main

//取得本帐户的所有域名列表的结构体
type DomainList struct {
	Status struct {
		Code       string
		Message    string
		Created_at string
	}
	Info struct {
		Domain_total int //域名总数
		All_total    int //域名总数
		//Mine_total      int    //自己创建的域名总数(不包括共享得到的域名)
		//Share_total     int //共享得到的域名总数
		//Vip_total       int
		//Ismark_total    int
		//Pause_total     int //暂停解析的域名总数
		//Error_total     int
		//Lock_total      int //已锁定的域名总数
		//Spam_total      int //已被封禁的域名总数
		//Vip_expire      int
		//Share_out_total int
	}
	Domains []Domain
}

//域名的结构体
type Domain struct {
	Id                int    //域名 ID, 即为 domain_id
	Status            string //status: 域名状态 “enable”: 正常,“pause”: 已暂停解析,“spam”: 已被封禁,“lock”: 已被锁定
	Grade             string
	Group_id          string
	Searchengine_push string
	Is_mark           string
	Ttl               string
	Cname_speedup     string
	Remark            string //remark: 域名备注
	Created_on        string
	Updated_on        string
	Punycode          string //使用 punycode 转码之后的域名
	Ext_status        string
	Name              string //域名
	Grade_title       string
	Is_vip            string
	Owner             string
	Records           string //域名下记录总条数
	Auth_to_anquanbao string
}

//取得域名的所有记录（子域名）
type GetRecordRet struct {
	Status struct {
		Code    string
		Message string
	}
	Domain struct {
		Id         string
		Name       string //"example.com",
		Punycode   string
		Grade      string
		Owner      string
		Ext_status string
		Ttl        int
	}
	Info struct {
		Sub_domains  string
		Record_total string
		Records_num  string
	}
	Records []Record
}

//记录（子域名）的结构体
type Record struct {
	Id             string
	Name           string
	Line           string
	Line_id        string
	Type           string //记录类型
	Ttl            string
	Value          string //IP地址
	Weight         string
	Mx             string
	Enabled        string
	Status         string
	Monitor_status string
	Remark         string
	Updated_on     string
	Use_aqb        string
}

//修改记录返回的结构体,如果1小时之内，提交了超过5次没有任何变动的记录修改请求，该记录会被系统锁定1小时，不允许再次修改。
/*

   共通返回
   -15 域名已被封禁
   -7 企业账号的域名需要升级才能设置
   -8 代理名下用户的域名需要升级才能设置
   6 域名ID错误
   7 不是域名所有者或没有权限
   8 记录ID错误
   21 域名被锁定
   22 子域名不合法
   23 子域名级数超出限制
   24 泛解析子域名错误
   500025 A记录负载均衡超出限制
   500026 CNAME记录负载均衡超出限制
   26 记录线路错误
   27 记录类型错误
   29 TTL 值太小
   30 MX 值错误，1-20
   31 URL记录数超出限制
   32 NS 记录数超出限制
   33 AAAA 记录数超出限制
   34 记录值非法
   35 添加的IP不允许
   36 @主机的NS纪录只能添加默认线路
   82 不能添加黑名单中的IP


*/
type RecordModifyRet struct {
	Status struct {
		Code       string //interface{}
		Message    string
		Created_at string
	}
	Record struct {
		Id     int    `json:"id"` //id: 记录ID, 即为 record_id
		Name   string //name: 子域名如www
		Value  string `json:"value"` //value”: 记录值，即修改的记录的IP地址
		Status string //status”: 记录状态
	}
}
