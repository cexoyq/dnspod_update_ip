package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

//TmpFilePath 保存ip到临时文件
//const TmpFilePath string = "/tmp/dnspod_ip"

//读文件，文件名称为"子域名.域名"
func r() (s string, err error) {
	//打开文件
	f, err := os.Open(f.TmpFile)
	if err != nil {
		fmt.Println("err = ", err)
		return
	}

	//关闭文件
	defer f.Close()

	buf := make([]byte, 32) //2k大小

	//n代表从文件读取内容的长度
	n, err := f.Read(buf)
	if err != nil && err != io.EOF { //文件出错，同时没有到结尾
		log.Println("读文件错误err = ", err)
		return
	}

	log.Println("读取文件buf = ", string(buf[:n]))
	return string(buf[:n]), err
}

//将IP写文件
func w(ip string) (err error) {
	//打开文件，新建文件
	f, err := os.Create(f.TmpFile) //传递文件路径
	if err != nil {                //有错误
		fmt.Println("err = ", err)
		return
	}

	//使用完毕，需要关闭文件
	defer f.Close()

	n, err := f.WriteString(ip)
	if err != nil {
		log.Println("保存IP，写入到临时文件出错，err = ", err)
	}
	fmt.Println("保存IP，写入到临时文件成功，写入字节数量： ", n)
	return err
}
